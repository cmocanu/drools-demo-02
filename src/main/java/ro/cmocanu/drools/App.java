package ro.cmocanu.drools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ro.cmocanu.drools.objects.Transaction;
import ro.cmocanu.drools.services.RulesService;

import javax.annotation.PreDestroy;
import java.math.BigDecimal;

@SpringBootApplication
public class App implements CommandLineRunner {

    private static final Logger LOG = LoggerFactory.getLogger(App.class);

    @Autowired
    private RulesService rulesService;

    public static void main(String args[]) {

        SpringApplication.run(App.class, args);

    }

    @PreDestroy
    public void shutdown() {
        LOG.info("Close KieSession pool");
        rulesService.shutdown();
    }

    @Override
    public void run(String... args) throws InterruptedException {
        LOG.info("EXECUTING : command line runner");

        Transaction txn10 = new Transaction();
        txn10.setTxnId(1L);
        txn10.setChannel("EFT");
        txn10.setFromAccount(1L);
        txn10.setToAccount(10L);
        txn10.setAmount(BigDecimal.valueOf(1000));
        txn10.setAcceptor("IDEALL.RO ONLINE");
        txn10.setMcc(4812L);
        rulesService.evaluateTransaction(txn10);

        Transaction txn14 = new Transaction();
        txn14.setTxnId(1L);
        txn14.setChannel("EFT");
        txn14.setFromAccount(1L);
        txn14.setToAccount(10L);
        txn14.setAmount(BigDecimal.valueOf(1000));
        txn14.setAcceptor("IDEAL TECH ONLINE");
        txn14.setMcc(4812L);
        rulesService.evaluateTransaction(txn14);

        Transaction txn17 = new Transaction();
        txn17.setTxnId(1L);
        txn17.setChannel("EFT");
        txn17.setFromAccount(1L);
        txn17.setToAccount(10L);
        txn17.setAmount(BigDecimal.valueOf(1000));
        txn17.setAcceptor("1IDEALL.RO");
        txn17.setMcc(4812L);
        rulesService.evaluateTransaction(txn17);

        Transaction txn18 = new Transaction();
        txn18.setTxnId(1L);
        txn18.setChannel("EFT");
        txn18.setFromAccount(1L);
        txn18.setToAccount(10L);
        txn18.setAmount(BigDecimal.valueOf(1000));
        txn18.setAcceptor("IDEALL.RO");
        txn18.setMcc(4812L);
        rulesService.evaluateTransaction(txn18);

        Transaction txn11 = new Transaction();
        txn11.setTxnId(1L);
        txn11.setChannel("EFT");
        txn11.setFromAccount(1L);
        txn11.setToAccount(10L);
        txn11.setAmount(BigDecimal.valueOf(1000));
        txn11.setAcceptor("DE LA PC  GARAGE MAG");
        txn11.setMcc(4812L);
        rulesService.evaluateTransaction(txn11);

        Transaction txn12 = new Transaction();
        txn12.setTxnId(1L);
        txn12.setChannel("EFT");
        txn12.setFromAccount(1L);
        txn12.setToAccount(10L);
        txn12.setAmount(BigDecimal.valueOf(1000));
        txn12.setAcceptor("CEL ONLINE");
        txn12.setMcc(4812L);
        rulesService.evaluateTransaction(txn12);

        Transaction txn15 = new Transaction();
        txn15.setTxnId(1L);
        txn15.setChannel("EFT");
        txn15.setFromAccount(1L);
        txn15.setToAccount(10L);
        txn15.setAmount(BigDecimal.valueOf(1000));
        txn15.setAcceptor("MAGAZIN CORSAR ONLINE SRL");
        txn15.setMcc(4812L);
        rulesService.evaluateTransaction(txn15);

        Transaction txn13 = new Transaction();
        txn13.setTxnId(1L);
        txn13.setChannel("EFT");
        txn13.setFromAccount(1L);
        txn13.setToAccount(10L);
        txn13.setAmount(BigDecimal.valueOf(1000));
        txn13.setAcceptor("MOBICEL ONLINE");
        txn13.setMcc(4812L);
        rulesService.evaluateTransaction(txn13);

        Transaction txn16 = new Transaction();
        txn16.setTxnId(1L);
        txn16.setChannel("EFT");
        txn16.setFromAccount(1L);
        txn16.setToAccount(10L);
        txn16.setAmount(BigDecimal.valueOf(1000));
        txn16.setAcceptor("EXCLUSIVE CEL");
        txn16.setMcc(4812L);
        rulesService.evaluateTransaction(txn16);
    }
}
