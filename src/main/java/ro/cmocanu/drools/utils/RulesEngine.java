package ro.cmocanu.drools.utils;

import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieRepository;
import org.kie.api.builder.ReleaseId;
import org.kie.api.io.Resource;
import org.kie.api.runtime.KieContainer;
import org.kie.internal.io.ResourceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class RulesEngine {

    private final static Logger LOG = LoggerFactory.getLogger(RulesEngine.class);

    @Autowired
    private KieServices kieServices;

    @Value("${drools.rules.filename}")
    private String rulesFilename;

    @Bean
    private KieServices kieServices() {
        return KieServices.Factory.get();
    }

    @Bean
    private KieContainer kieContainer(KieServices kieServices) {
        Resource dt = ResourceFactory.newFileResource(rulesFilename);

        KieFileSystem kieFileSystem = kieServices.newKieFileSystem().write(dt);

        KieBuilder kieBuilder = kieServices.newKieBuilder(kieFileSystem);
        kieBuilder.buildAll();

        KieRepository kieRepository = kieServices.getRepository();

        ReleaseId krDefaultReleaseId = kieRepository.getDefaultReleaseId();

        return kieServices.newKieContainer(krDefaultReleaseId);
    }
}
